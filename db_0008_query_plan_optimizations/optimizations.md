
1)
![img.png](1.png)
Здесь можно заметить, что у Postgre возникают проблемы с оценкой прохода вложенным циклом. Postgre думает, что объем данных, которых нужно будет пройти, будет небольшим, а на деле декартово произведение оказалось очень даже не малым. Postgres ошибся аж в 62 раза, из-за чего выбранный способ оказался далеко не лучшим. Вложенные циклы хороши, когда две таблицы маленькие, ну или хотя бы одна (лучше если от которой идет цикл), потому что A*B. При таких объемах хэши подойдут намного лучше, в чем можно убедиться, задав set enable_nestloop = off;  Может быть проблема в нехватке памяти для создания хэша? Но тут все в порядке, никаких дополнительных файлов при отключенном nested loop не создается - с памятью проблем нет.
Можно оптимизировать запрос, навесив индекс(b-tree, так как столбец aircraft_code в flights далеко не уникальный) на foreign key flights.aircraft_code.
CREATE INDEX ON flights(aircraft_code);
Тогда соединение flights и aircrafts будет происходить по индексу с использованием битовой карты между деревом и страницами. Битовая карта здесь нужна, потому что aircraft_code далеко не уникален в таблице flights
Такая оптимизация здесь оправдана, тк время выполнения запроса уменьшается в десятки раз
2)
![img.png](2.png)
Здесь проблемы с оценкой уже нет, реальная стоимоть наоборот в разы меньше, чем постгре оценил - использование хэш таблиц здесь выглядит оправданно 
![img.png](3.png)
Но также можно заметить, что в запросе используется order by, следовательно полученные данные после хэша здесь еще и сортируются quicksort-ом.
При order by можно использовать merge sort - строчки примут отсортированный вид + нет расходов памяти на создание хэш таблиц
Попробуем отключить hashaggreagate - set enable_hashagg to off;
Теперь постгре стал использовать merge sort, но время запроса увеличилось, следовательно, объем извлекаемых данных большой, тк при большом объеме данных хэширование работает лучше
Создавать какие-то индексы здесь смысла нет - оптимизировать здесь нечего

3)
![img.png](4.png)
Здесь видно,  что для хэш-таблицы создается 7 временных файлов + 1 в оперативке (оперативной памяти не хватает) - поднимаем mem_size ровно на столько, чтобы в этом месте значение Batches было равно 1. В данном случае 28мб оказалось достаточно.
Это вполне адекватная оптимизация, оперативная память в постгре по умолчанию выставлена очень низко, чтобы можно было запускать постгре на любом сервере, 28 мб много не будет)

4)
![img.png](5.png)
Здесь видно, что для хэш-таблицы создается 4 временных файла + 1 в оперативке (оперативной памяти не хватает - работа с временными файлами дает нагрузку) - поднимаем ровно на столько, чтобы в этом месте значение Batches было равно 1. В данном случае 12 мб оказалось достаточно. Время на выполнение сократилось в 1.5 раза (среднее из 50 запросов for i in 1..50 loop)

5)
Здесь опять видим nested loops, оценки которых занижены, кол-во строк в 50 раз больше получилось:
![img.png](6.png)
![img.png](7.png)

Следовательно, отключаем nested loop:
set enable_nestloop = off;
Видим, что время сократилось на четверть - смотрим,  какие индексы можно повесить
![img.png](8.png)
Еще здесь идет цикл проверки по индексу 310к раз, при таком объеме легче пройтись seq scan-ом, что и подтверждается, если отключить индексы
